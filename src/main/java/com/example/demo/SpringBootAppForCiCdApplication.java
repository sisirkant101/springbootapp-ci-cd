package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootAppForCiCdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAppForCiCdApplication.class, args);
	}
	
	@GetMapping("/welcome")
	public String getMessage() {
		return "welcome to Ramphal Technologies pvt.ltd";
	}

}
